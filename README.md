# OCaml Template

This is a simple example for an OCaml application, showing just how easy it is
to get up and running with OCaml development using GitLab.

## Reference links

- [GitLab CI Documentation][gitlab-ci]
- [Dune quickstart][dune]

[gitlab-ci]: https://docs.gitlab.com/ee/ci/
[dune]: https://dune.readthedocs.io/en/latest/quick-start.html

If you're new to OCaml you will want to check out [the tutorial][learn], but if
you're already a seasoned developer considering building your own OCaml
application with Dune and GitLab, this should all look very familiar.

[learn]: https://ocaml.org/docs

## What is contained in this repository?

This repository contains the output of `dune init proj`, which generates an
empty application project, with all of the Dune setup ready to go. A simple
executable that prints "Hello world!" is given as an example.

In addition to the Dune project itself, the repository comes with a `.gitignore`
file that excludes build files and OPAM switch files.

Finally, the `.gitlab-ci.yml` file contain a minimal pipeline that compiles and
runs the unit tests of the project.

## Usage

### Prerequisites

Make sure you have [OPAM][opam] installed.

[opam]: https://opam.ocaml.org/

### Setup

In order to setup your development environment, follow the steps below.

- Create a new OCaml switch.

```bash
opam switch create . 5.1.0
```

- Pin the package.

```bash
opam pin --with-test --with-doc .
```

- You can check that the setup works by running the main executable.

```bash
dune exec ocaml_template
```
